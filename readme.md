# Open-source IDP for AWS Access

This is an alternative approach to using Windows AD Federation Services for controlling AWS IAM access in your organization.

## Sample Use Case
You need to provide IAM privileges to both internal users and external vendors, with the following restrictions:
  * you have multiple AWS accounts (dev, staging, prod)
  * you want each external vendor to use their own custom login URL
  * you want the ability to use different email domains for login
  * you want the flexibility to manage users without dependency on proprietary software

## Technology Overview

The IDP system uses an SQL data store.  This allows for the flexibility to build custom apps/integrations for user management (i.e. password reset tool, remote user lock/unlock)
